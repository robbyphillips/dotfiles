# About
This is my dotfiles repository.

It is structured here to be used with GNU Stow, a symlinking utility.

This README also contains some notes to myself on getting set up again because
it doesn't happen often enough for me to remember.

## Contents
1. first things first
2. zsh
3. vim
4. x-archlinux
5. x-ubuntu (?? coming eventually??)
6. More stuff coming... eventually.

## first things first
1. Clone this repo to `~/dotfiles`
2. get `stow`
3. "Install" dotfiles using `stow -S <package>` where `<package>` is one of the
   folders in dotfiles like 'zsh'.
4. To verify that everything is going to be put where it is supposed to be without
   actually making any changes, use the `-n` and `--verbose[=n]` flags.
   E.g. `stow -n --verbose=2 -S zsh`.

## zsh
Steps to get up and running again:

1. get zsh ...
2. get oh-my-zsh ...
3. `cd ~/dotfiles`
4. `stow -S zsh`

## vim
Steps to get up and running again:

1. verify vim installed
2. get vundle
3. `mkdir ~/.vim/backup ~/.vim/tmp`
4. `cd ~/dotfiles`
5. `stow -S vim`
6. run `vim`
7. in vim, run `:PluginInstall`

## x-archlinux

This just has a couple `.X*` files that were tailored to an old archlinux setup.  
Might be useful again at some point, but are really just here for posterity.

## x-ubuntu

Just made the transition to ubuntu.  Haven't made many tweaks yet, but this is
likely where those configurations will live.
