
call plug#begin('~/.local/share/nvim/plugged')

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugs' }
Plug 'itchyny/lightline.vim'

Plug 'Align'
Plug 'airblade/vim-gitgutter'
Plug 'clang'
Plug 'delimitMate.vim'
Plug 'elixir-lang/vim-elixir'
Plug 'fatih/vim-go'
Plug 'jellybeans.vim'
Plug 'kchmck/vim-coffee-script'
Plug 'ledger/vim-ledger'
Plug 'mattboehm/vim-accordion'
Plug 'molokai'
Plug 'obsidian'
Plug 'rust-lang/rust.vim'
Plug 'scrooloose/syntastic'
Plug 'SirVer/ultisnips'
Plug 'slashmili/alchemist.vim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
" Plug 'tpope/vim-obsession'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vimwiki/vimwiki'

call plug#end()

set autoread
set autochdir
set hidden
set nohlsearch
set wildmenu                " command line completion
set wildignorecase          " ignore case for completions on cmd line
set undodir=~/.local/share/nvim/undodir
set backupdir=~/.local/share/nvim/backup
set virtualedit=block

" ## Lightline
let g:lightline = {
      \ 'colorscheme': 'powerline',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'fugitive', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&filetype=="help"?"":&readonly?"x":""}',
      \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
      \   'fugitive': '%{exists("*fugitive#head")?fugitive#head():""}'
      \ },
      \ 'component_visible_condition': {
      \   'readonly': '(&filetype!="help"&& &readonly)',
      \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
      \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '|', 'right': '|' }
      \ }

" ## DelimitMate
let delimitMate_expand_cr = 1
let delimitMate_expand_space = 1

" ## Ledger
let g:ledger_maxwidth = 80
let g:ledger_fillstring = '    ' 

" ## Ultisnips
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" # Key Mappings
let mapleader = ","         " map leader key to the standard and useful comma

" Alt escapes from insert mode [`^ jumps to where insert mode was exited]
imap kk <esc>`^
imap jj <esc>`^
imap kj <esc>`^
imap jk <esc>`^

" faster inter-window movments [j and k conflict with ultisnips?]
" map <C-j> <C-w>j
" map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-h> <C-w>h

" quick window splits
nmap <leader>v <C-w>v
nmap <leader>s <C-w>s

" resize window splits [conflict with osx change desktop]
nmap <C-left> <C-w><
nmap <C-right> <C-w>>

" Quick yank/paste with system clipboard 
nnoremap <leader>Y "*Y
nnoremap <leader>y "*y
nnoremap <leader>p "*p

" reload .vimrc
nmap <leader>R :so $MYVIMRC<CR>:echo "reloaded:" $MYVIMRC<CR>

" open .vimrc
nmap <leader>V :e $MYVIMRC<CR>

" toggle spell checking
nmap <silent> <leader>S :setlocal spell!<CR>

" Enter current time and date
inoremap <F8> <C-R>=strftime("%Y-%m-%d")<CR>
nnoremap <F8> "=strftime("%Y-%m-%d")<CR>P
inoremap <F9> <C-R>=strftime("%c")<CR>
nnoremap <F9> "=strftime("%c")<CR>P
inoremap <F10> <C-R>=strftime("%b %d %Y")<CR>
nnoremap <F10> "=strftime("%b %d %Y")<CR>P

" Insert date in the form yyyymmdd on the command line
" [useful for date-stamped file names]
cnoremap <F10> <C-R>=strftime("%Y%m%d")<CR>

" ,r to replace once, . to repeat
" works only on recent patches, > 7xx
nnoremap <leader>0 :let @/ = expand('<cword>')<cr>
nmap <leader>r <leader>0cgn


" # Auto Commands
" Highlight the column one to the right of textwidth
" autocmd BufEnter * set colorcolumn=+1
autocmd FileType gitcommit setlocal textwidth=72

autocmd FileType go setlocal ts=4 sts=0 sw=4 noexpandtab

" Auto compile coffee script files on write
autocmd BufWritePost *.coffee silent make!

" Set standard 2 space shift for coffee files
autocmd BufNewFile,BufReadPost *.coffee setl shiftwidth=2 expandtab
