" .vimrc
"
"
set nocompatible            " No reason to care about old vi
filetype off                " required [for vundle to start correctly?]

 " let vundle handle my plugins
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()            " start up vundle


" # Plugins :
 Plugin 'VundleVim/Vundle.vim'

 Plugin 'Align'
 Plugin 'airblade/vim-gitgutter'
 " Plugin 'bling/vim-airline'
 Plugin 'vim-airline/vim-airline'
 Plugin 'vim-airline/vim-airline-themes'
 Plugin 'clang'
 " Plugin 'dart-lang/dart-vim-plugin'
 Plugin 'delimitMate.vim'
 Plugin 'dikiaap/minimalist'
 Plugin 'editorconfig/editorconfig-vim'
 " Plugin 'elixir-lang/vim-elixir'
 Plugin 'ekalinin/Dockerfile.vim'
 Plugin 'fatih/vim-go'
 Plugin 'honza/vim-snippets'
 " Plugin 'HerringtonDarkholme/yats.vim'
 Plugin 'L9'
 Plugin 'ledger/vim-ledger'
 " Plugin 'leafgarland/typescript-vim'
 " Plugin 'mattboehm/vim-accordion'
 " Plugin 'mhinz/vim-startify'
 Plugin 'moll/vim-node'
 Plugin 'molokai'
 " Plugin 'obsidian'
 Plugin 'pangloss/vim-javascript'
 Plugin 'Quramy/vim-js-pretty-template'
 " Plugin 'rust-lang/rust.vim'
 Plugin 'scrooloose/syntastic'
 Plugin 'SirVer/ultisnips'
 " Plugin 'slashmili/alchemist.vim'
 Plugin 'Solarized'
 " Plugin 'sophacles/vim-processing'
 " Plugin 'suan/vim-instant-markdown'
 Plugin 'tpope/vim-commentary'
 Plugin 'tpope/vim-fugitive'
 Plugin 'tpope/vim-markdown'
 " Plugin 'tpope/vim-obsession'
 Plugin 'tpope/vim-repeat'
 Plugin 'tpope/vim-speeddating'
 Plugin 'tpope/vim-surround'
 Plugin 'tpope/vim-unimpaired'
 Plugin 'vimwiki/vimwiki'
 Plugin 'briancollins/vim-jst'

call vundle#end()           " required!
filetype plugin indent on   " required! 

runtime ftplugin/man.vim    " activate man page reader plugin

" # Plugin Settings
" ## Airline
let g:airline_theme='base16_spacemacs'
let g:airline_powerline_fonts = 0
let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#tabline#enabled = 0
" give me short mode labels
let g:airline_mode_map = {
  \ '__' : '-',
  \ 'n'  : 'N',
  \ 'i'  : 'I',
  \ 'R'  : 'R',
  \ 'c'  : 'C',
  \ 'v'  : 'V',
  \ 'V'  : 'V',
  \ '' : 'V',
  \ 's'  : 'S',
  \ 'S'  : 'S',
  \ '' : 'S',
  \ }

" ## DelimitMate
let delimitMate_expand_cr = 1
let delimitMate_expand_space = 1

" ## Ledger
let g:ledger_maxwidth = 80
let g:ledger_fillstring = '    ' 

" ## Ultisnips
" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" # Preferences
" ## Functionality
set autochdir               " automatically change to current file's directory
set autoread                " automatically read in a file if it has changed
set autowrite               " automatically write changes when leaving buffer
set backup		    " make backups
set backupdir=~/.vim/backup " directory for backups
set directory=~/.vim/tmp    " directory for swapfiles [or set noswapfile]
set hidden                  " allow hidden buffers; switch buffers w/o saving
set ignorecase              " ignore case when searching
set incsearch               " search incrementally
set smartcase               " disable ignorecase in search if upcase letters typed
set tags+=./tags;$HOME
set wildmenu                " command line completion
set wildignorecase          " ignore case for completions on cmd line
set virtualedit=block       " allow the cursor to move to 'invalid' places
set cryptmethod=blowfish    " set default encryption method to blowfish

" ## Formatting
set linebreak               " do not chop words when a new line is needed
set textwidth=80            " allow text to be # chars wide

" ## Whitespace
set tabstop=4               " default number of spaces to indent
set softtabstop=4           " actually use this to indent
set shiftwidth=4            " number of spaces to auto-indent
set expandtab               " uses spaces in place of tab
set smarttab                " better tabbing; use shiftwidth, not tabstop

" ## Display Options
syntax enable               " syntax highlighting is important
set laststatus=2            " always show status line
set noeb vb t_vb=           " no error bells
set nohlsearch              " do not highlight search results
set number                  " give me line numbers
set relativenumber
set nowrap                  " do not wrap lines... I'm breaking them anyway
set ruler                   " show the row,column and % of doc in status
set scrolloff=3             " keep # lines for context when scrolling
set showcmd                 " show partially completed cmds at bottom of win
" set background=dark         " cave dwelling
" colorscheme molokai


" return the syntax highlight group under the cursor ''
function! StatuslineCurrentHighlight()
    let name = synIDattr(synID(line('.'),col('.'),1),'name')
    if name == ''
        return ''
    else
        return '[' . name . ']'
    endif
endfunction

" # Conditional Preferences
if has('gui_running')
    " get rid of all the random gui things..
    set guioptions=
    colorscheme molokai
    "set guifont=Source\ Code\ Pro\ for\ Powerline:h11
    " set guifont=Anonymice\ Powerline:h12
endif 

" # Key Mappings
let mapleader = ","         " map leader key to the standard and useful comma

" logical cursor movements for when lines do wrap
nnoremap j gj
nnoremap k gk

" Alt escapes from insert mode [`^ jumps to where insert mode was exited]
imap kk <esc>`^
imap jj <esc>`^
imap kj <esc>`^
imap jk <esc>`^

" behaves similar to a web browser for reading..
nmap <space> <PageDown>
nmap <S-space> <PageUp>

" faster inter-window movments [j and k conflict with ultisnips?]
" map <C-j> <C-w>j
" map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-h> <C-w>h

" easier scrolling
nnoremap <D-j> <C-d>
nnoremap <D-k> <C-u>

" quick window splits
nmap <leader>v <C-w>v
nmap <leader>s <C-w>s

" resize window splits [conflict with osx change desktop]
nmap <C-left> <C-w><
nmap <C-right> <C-w>>

" Quick yank/paste with system clipboard 
nnoremap <leader>Y "*Y
nnoremap <leader>y "*y
nnoremap <leader>p "*p

" reload .vimrc
nmap <leader>R :so $MYVIMRC<CR>:echo "reloaded:" $MYVIMRC<CR>

" open .vimrc
nmap <leader>V :e $MYVIMRC<CR>

" toggle spell checking
nmap <silent> <leader>S :setlocal spell!<CR>

" Enter current time and date
inoremap <F8> <C-R>=strftime("%Y-%m-%d")<CR>
nnoremap <F8> "=strftime("%Y-%m-%d")<CR>P
inoremap <F9> <C-R>=strftime("%c")<CR>
nnoremap <F9> "=strftime("%c")<CR>P
inoremap <F10> <C-R>=strftime("%b %d %Y")<CR>
nnoremap <F10> "=strftime("%b %d %Y")<CR>P

" Insert date in the form yyyymmdd on the command line
" [useful for date-stamped file names]
cnoremap <F10> <C-R>=strftime("%Y%m%d")<CR>

" ,r to replace once, . to repeat
" works only on recent patches, > 7xx
nnoremap <leader>0 :let @/ = expand('<cword>')<cr>
nmap <leader>r <leader>0cgn


" # Auto Commands
" Highlight the column one to the right of textwidth
" autocmd BufEnter * set colorcolumn=+1
autocmd FileType gitcommit setlocal textwidth=72

autocmd FileType go setlocal ts=8 sts=0 sw=8 noexpandtab

" Set ruby whitespace formatting
autocmd FileType ruby setlocal ts=2 sts=2 sw=2 expandtab smarttab
